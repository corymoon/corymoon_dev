import adapter from '@sveltejs/adapter-auto'
import preprocess from 'svelte-preprocess'
import svg from '@poppanator/sveltekit-svg'

const svgPlugin = svg({
    svgoOptions: {
        multipass: true,
        plugins: [
            {
                name: 'preset-default',
                params: {
                    overrides: {
                        removeViewBox: false,
                    },
                },
            },
            'removeDimensions',
        ],
    },
})

/** @type {import('@sveltejs/kit').Config} */
const config = {
    kit: {
        floc: process.env.NODE_ENV === 'development',
        adapter: adapter(),
        methodOverride: {
            allowed: ['PATCH', 'DELETE'],
        },
        vite: {
            plugins: [svgPlugin],
            define: {
                'import.meta.env.VERCEL_ANALYTICS_ID': JSON.stringify(
                    process.env.VERCEL_ANALYTICS_ID,
                ),
            },
        },
    },
    preprocess: [
        preprocess({
            postcss: true,
        }),
    ],
}

export default config
