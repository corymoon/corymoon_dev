import { Client } from '@notionhq/client'
import { NotionToMarkdown } from 'notion-to-md'

import type {
    Project,
    NotionProjectResult,
    ProjectStatus,
    TechnologiesResultArray,
    MyTech,
} from '$lib/types'

const isNotionResult = (result: Record<string, unknown>): result is NotionProjectResult =>
    !!(
        (result as NotionProjectResult)?.properties.name.title[0].plain_text &&
        (result as NotionProjectResult)?.properties.image.files[0].file.url &&
        (result as NotionProjectResult)?.properties.description.rich_text[0].plain_text &&
        (result as NotionProjectResult)?.properties.repoUrl.url !== undefined &&
        (result as NotionProjectResult)?.properties
    )

const mapTechnologiesToTech = (technologiesArray: TechnologiesResultArray) => {
    if (technologiesArray.length === 0) {
        return []
    }
    return technologiesArray.map((t) => {
        return t.name as MyTech
    })
}

const mapResultToProject = (notionResult: Record<string, unknown>): Project | null => {
    return isNotionResult(notionResult)
        ? {
              name: notionResult.properties.name.title[0].plain_text,
              image: notionResult.properties.image.files[0].file.url,
              description: notionResult.properties.description.rich_text[0].plain_text,
              repo: notionResult.properties.repoUrl.url,
              id: notionResult.id,
              tech: mapTechnologiesToTech(
                  notionResult.properties.technologies.multi_select as TechnologiesResultArray,
              ),
              url: notionResult.properties.url.url,
            status: notionResult.properties.status.status.name as ProjectStatus,
              content: ''
          }
        : null
}

class ProjectsRepo {
    #client = new Client({
        auth: import.meta.env.VITE_NOTION_SECRET,
    })

    getAll = async ({ limit }: { limit: number }) => {
        const { results } = await this.#client.databases.query({
            database_id: import.meta.env.VITE_NOTION_DB_ID,
            page_size: limit,
        })

        return results.map(mapResultToProject).filter(Boolean)
    }

    getProjectById = async (id: string) => {
        const n2m = new NotionToMarkdown({ notionClient: this.#client })
        const response = await this.#client.pages.retrieve({ page_id: id })

        const mdblocks = await n2m.pageToMarkdown(id)

        const project = mapResultToProject(response)
        project!.content = n2m.toMarkdownString(mdblocks)
        

        return project
    }
}

export const projectsRepo = new ProjectsRepo()
