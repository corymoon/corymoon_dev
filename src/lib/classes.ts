export const projectUrlLinkClasses: string = 'drac-text-purple drac-text-purple-cyan--hover'
export const projectRepoLinkClasses: string = 'drac-text-pink drac-text-yellow-pink--hover'
