import type { MyTech } from './types'

export const makeBadge = (
    color: string,
    tech: string,
    logoColor: string | undefined = undefined,
) => {
    let url = `https://img.shields.io/badge/${tech}-%23${color}.svg?&style=for-the-badge&logo=${tech}`
    if (logoColor != undefined) {
        url = url + `&logoColor=%23${logoColor}`
    }
    return url
}

export const techColor = (tech: MyTech) => {
    switch (tech) {
        case 'Blender':
            return '#E27203'
        case 'Dart':
            return '#28ADE9'
        case 'Firebase':
            return '#F29E10'
        case 'Flutter':
            return '#5ABFEB'
        case 'Reaper':
            return '#63B830'
        case 'Svelte':
        case 'SvelteKit':
            return '#F23B00'
        case 'TailwindCSS':
            return '#18B4C1'
        case 'TypeScript':
            return '#0074C1'
        case 'Notion':
            return '#434449'
        case 'Disco Diffusion':
            return '#F9AB00'
        case 'Python':
            return '#275378'
        case 'Vercel':
        default:
            return '#000'
    }
}
