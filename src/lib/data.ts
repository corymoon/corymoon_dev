export const name: string = 'Cory Moon'
export const email: string = 'corymoon13@gmail.com'
export const web: string = 'corymoon.dev'

const siteTitle: string = name.toLowerCase() + ' // ' + web

export const things: string[] = ['🎓 student', '🛠 maker', '🐩 poodle dad']

export const makeSiteTitle = (subtitle: string | undefined) => {
    if (subtitle == undefined || subtitle == '') {
        return siteTitle
    } else {
        return subtitle + ' || ' + siteTitle
    }
}
