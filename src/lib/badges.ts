import { makeBadge } from './helpers'

type badge = {
    link: string
    img: string
    alt: string
}

export const badges: badge[] = [
    {
        link: 'https://dart.dev/',
        img: makeBadge('0175C2', 'dart'),
        alt: 'Dart badge',
    },
    {
        link: 'https://www.python.org/',
        img: makeBadge('14354C', 'python', 'ffffff'),
        alt: 'Python badge',
    },
    {
        link: 'https://sass-lang.com/',
        img: makeBadge('bf4080', 'SASS', 'ffffff'),
        alt: 'Sass badge',
    },
    {
        link: 'https://firebase.google.com/',
        img: makeBadge('039BE5', 'firebase'),
        alt: 'Firebase badge',
    },
    {
        link: 'https://vuejs.org/',
        img: makeBadge('35495e', 'vue.js', '4FC08D'),
        alt: 'Vue badge',
    },
    {
        link: 'https://go.dev/',
        img: makeBadge('00ADD8', 'go', 'ffffff'),
        alt: 'Go badge',
    },
    {
        link: 'https://svelte.dev/',
        img: makeBadge('FF3B00', 'svelte', 'ffffff'),
        alt: 'Go badge',
    },
]
