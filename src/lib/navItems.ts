export type navItem = {
    href: string
    label: string
}

export const navItems: navItem[] = [
    {
        href: '/',
        label: 'Home',
    },
    {
        href: '/me',
        label: 'Me',
    },
    {
        href: '/projects',
        label: 'Projects',
    },
    {
        href: 'https://gitlab.com/corymoon',
        label: 'GitLab',
    },
]
