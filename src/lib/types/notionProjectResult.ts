export type NotionProjectResult = {
    icon: {
        emoji: string
    }
    id: string
    properties: {
        repoUrl: {
            type: 'url'
            url: string
        }
        description: {
            type: 'rich_text'
            rich_text: {
                plain_text: string
            }[]
        }
        url: {
            type: 'url'
            url: string
        }
        name: {
            type: 'title'
            title: {
                type: 'text'
                plain_text: string
            }[]
        }
        status: {
            type: 'status'
            status: {
                name: string
            }
        }
        technologies: {
            type: 'multi_select'
            multi_select: {
                name: string
            }[]
        }
        image: {
            type: 'files'
            files: {
                type: 'file'
                file: {
                    url: string
                }
            }[]
        }
    }
}
