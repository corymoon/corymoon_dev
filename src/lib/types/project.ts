import type { MyTech } from "./myTech"

export type Project = {
    id: string
    name: string
    url: string
    repo: string | null
    description: string
    tech: MyTech[]
    image: string
    status: ProjectStatus
    content: string
}

export type ProjectStatus = 'Published' | 'In progress' | 'Not started' | 'Personal'

export type TechnologiesResultArray = { id: string; name: string; color: string }[]
