import { projectsRepo } from '$lib/repos/projects'
import type { RequestHandler } from '@sveltejs/kit'

export const get: RequestHandler = async ({ url }) => {
    const limit = parseInt(url.searchParams.get('limit') || '50')
    const projects = await projectsRepo.getAll({ limit })

    return {
        status: 200,
        headers: {
            'access-control-allow-origin': '*',
        },
        body: { projects },
    }
}
