// import { getProjectById } from '$lib/repos/projects'
import { projectsRepo } from '$lib/repos/projects'
import type { RequestHandler } from './__types/[id]'

export const get: RequestHandler = async ({ params }) => {
    const { id } = params
    const project = await projectsRepo.getProjectById(id)
    // const project = {}

    if (!project) {
        return { status: 404 }
    }

    return { body: { project } }
}
