# my website
Started as the default SvelteKit tempalte from [vercel](https://vercel.com/) with [dracula ui](https://draculatheme.com/ui) and [tailwind css](https://tailwindcss.com/) (as [daisyui](https://daisyui.com/)) for the styling.

> ## Developing
> 
> Once you've created a project and installed dependencies with `pnpm install`, start a development server:
> 
> ```bash
> pnpm run dev
> 
> # or start the server and open the app in a new browser tab
> pnpm run dev -- --open
> ```
> 
> ## Building
> 
> To create a production version of your app:
> 
> ```bash
> pnpm run build
> ```
> 
> You can preview the production build with `npm run preview`.
