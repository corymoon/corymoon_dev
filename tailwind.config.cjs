/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ['./src/**/*.{html,js,svelte,ts}'],
    theme: {
        colors: {
            white: '#F8F8F2',
            cyan: '#80FFEA',
            green: '#8AFF80',
            orange: '#FFCA80',
            pink: '#FF80BF',
            purple: '#9580FF',
            red: '#FF9580',
            yellow: '#FFFF80',
            black: '#22212C',
            comment: '#7970A9',
            selection: '#454158',
        },
        fontFamily: {
            sans: ['Fira Code', 'monospace'],
        },
        extend: {},
    },
    plugins: [require('daisyui'), require('@tailwindcss/typography')],
}
